var functions = require('firebase-functions');
var admin = require('firebase-admin');
admin.initializeApp();
var { Storage } = require('@google-cloud/storage');
var axios = require('axios');
var FormData = require('form-data');
const { initializeApp } = require('firebase-admin');
var storage = new Storage();
axios.interceptors.request.use(request => {
    console.log(request);
    return request
});



const SIGNATURES_QUANTITY_LIMIT = 1;
const TIMESTAMPS_SIGNATURES_QUANTITY_LIMIT = 1;
const FINAL_CERTIFICATES_QUANTITY_LIMIT = 5;
const TOTAL_OF_REQUIRED_VALID_SUBJECTS = 3;


function sendFileToValidate(fileContent, filename) {
    var data = new FormData();
    data.append('document', fileContent, {
        filename: filename,
        contentType: 'application/octet-stream',
    });
    var config = {
        method: 'post',
        url: 'https://vol.uanataca.com/api/documents/',
        headers: {
            'Authorization': 'Basic b3NhbnRpZXN0ZWJhbkBkaWdpY2VydDpkYWZmeTc4Nw==',
            ...data.getHeaders()
        },
        data: data
    };

    axios(config)
        .then(function(response) {
            if (response.status !== 201) {
                console.log('The VOL response is not 201, check the file');
                return;
            }
            handleResponseOfSubmittedDocument(response.data);
        })
        .catch(function(error) {
            console.log(error);
        });
}

function handleResponseOfSubmittedDocument(documentSentResponse) {
    const initialResponse = {
        code: documentSentResponse.code,
        document_id: documentSentResponse.location.substring(11)
    };
    fetchFinalValidation(initialResponse.document_id);
}

function fetchFinalValidation(documentId) {
    const config = {
        method: 'get',
        url: `https://vol.uanataca.com/api/documents/${documentId}/verify/report?type=json&template=report&lang=es`,
        headers: {
            'Authorization': 'Basic b3NhbnRpZXN0ZWJhbkBkaWdpY2VydDpkYWZmeTc4Nw==',
        },
    }
    axios(config)
        .then(response => {
            if (response.status !== 200) {
                console.log('The VOL final response is not 200');
                return;
            }
            handleFileValidationResponse(response.data);
        })
        .catch(error => console.log(error));
}

function handleFileValidationResponse(validationResponse) {
    const validationResult = validationResponse.validation.result;
    console.log("Validando documento de manera general...");
    if (validationResult != "SUCCESS") {
        console.log("La respuesta de la validación en general no es válida, no se puede continuar");
        return;
    }
    const totalOfSignatures = validationResponse.validation.signatures.length;
    if (totalOfSignatures > SIGNATURES_QUANTITY_LIMIT) {
        console.log(`Se encontrarón más de ${SIGNATURES_QUANTITY_LIMIT} firmas, no se puede continuar`);
        return;
    }
    if (totalOfSignatures < SIGNATURES_QUANTITY_LIMIT) {
        console.log(`No se encontrarón la cantidad de firmas requeridas, esperadas:${SIGNATURES_QUANTITY_LIMIT}, encontradas: ${totalOfSignatures}`);
        return;
    }

    console.log("Validando la firma del documento...");
    const signatureStatus = validationResponse.validation.signatures[0].status;
    const signatureIntegrityValidation = signatureStatus[""];
    console.log("Validando la integridad de la firma del documento...");
    if (signatureIntegrityValidation.length > 1) {
        console.log(`Se encontrarón mas de 1 posible valor en la validación de integridad de la firma, no se puede continuar: ${signatureIntegrityValidation.toString()}`);
        return;
    }
    if (signatureIntegrityValidation[0] != "SUCCESS:SIGNATURE_VERIFIED") {
        console.log("No se pudo verificar la integridad de la firma, no se puede continuar");
        return;
    }
    console.log("Pasó la validación de integridad.");

    console.log("Validando la cadena de confianza con la que se firmo el documento...");
    const certificateValidation = signatureStatus[".validation.signatures[0].certificates[1]"];
    if (certificateValidation.length > 1) {
        console.log(`Se encontrarón mas de 1 posible valor en la validación de la cadena de confianza del certificado, no se puede continuar: ${certificateValidation.toString()}`);
        return;
    }
    if (certificateValidation[0] != "SUCCESS:TRUSTED") {
        console.log("No se pudo verificar la cadena de confianza del certificado, no se puede continuar");
        return;
    }
    console.log("Pasó la validación de la cadena de confianza con la que se firmó el documento");

    console.log("Validando la firma con sellado de tiempo...");
    const timestampValidation = signatureStatus[".validation.signatures[0].timestamps[0]"];
    if (timestampValidation.length > 1) {
        console.log(`Se encontrarón mas 1 posible valor en la validación de la integridad del sello de tiempo, no se puede continuar: ${timestampValidation.toString()}`);
        return;
    }
    if (timestampValidation[0] != "SUCCESS:SIGNATURE_TIMESTAMP_OK") {
        console.log("No se pudo verificar la integridad del sello de tiempo, no se puede continuar");
        return;
    }
    console.log("Pasó la validación de la firma de sello de tiempo");

    console.log("Validando el sello de tiempo del documento...");
    const timestampsSignature = validationResponse.validation.signatures[0].timestamps;
    if (timestampsSignature.length > TIMESTAMPS_SIGNATURES_QUANTITY_LIMIT) {
        console.log(`Se encontrarón más de ${TIMESTAMPS_SIGNATURES_QUANTITY_LIMIT} sellos de tiempo, no se puede continuar`);
        return;
    }
    if (timestampsSignature.length < TIMESTAMPS_SIGNATURES_QUANTITY_LIMIT) {
        console.log(`No se encontrarón la cantidad de sellos de tiempo requeridas, esperadas:${TIMESTAMPS_SIGNATURES_QUANTITY_LIMIT}, encontradas: ${timestampsSignature.length}`);
        return;
    }

    console.log("Validando la integridad del sello de tiempo...");
    const timestampStatus = timestampsSignature[0].status;
    const timestampIntegrityValidation = timestampStatus[""];
    if (timestampIntegrityValidation.length > 1) {
        console.log(`Se encontrarón mas de 1 posible valor en la validación de la integridad del sellado de tiempo, no se puede continuar: ${timestampIntegrityValidation.toString()}`);
        return;
    }
    if (timestampIntegrityValidation[0] != "SUCCESS:TIMESTAMP_VERIFIED") {
        console.log("No se pudo verificar la integridad del sello de tiempo, no se puede continuar");
        return;
    }
    console.log("Pasó la validación de la integridad de sello de tiempo");

    console.log("Validando la cadena de confianza del sello de tiempo...");
    const timestampCertificateValidation = timestampStatus[".validation.signatures[0].timestamps[0].certificates[1]"];
    if (timestampCertificateValidation.length > 1) {
        console.log(`Se encontró mas de 1 posible valor en la validación de la cadena de confianza del certificado de sello de tiempo, no se puede continuar: ${timestampCertificateValidation.toString()}`);
        return;
    }
    if (timestampCertificateValidation[0] != "SUCCESS:TRUSTED") {
        console.log("No se pudo validar la cadena de confianza del certificado de sello de tiempo, no se puede continuar");
        return;
    }
    console.log("Pasó la validación de la cadena de confianza del sello de tiempo");

    console.log("Validando los certificados encontrados en la firma...");
    if (validationResponse.report.signatures.length > SIGNATURES_QUANTITY_LIMIT) {
        console.log(`Se encontrarón más de ${SIGNATURES_QUANTITY_LIMIT} firmas, no se puede continuar`);
        return;
    }
    if (validationResponse.report.signatures.length < SIGNATURES_QUANTITY_LIMIT) {
        console.log(`No se encontrarón la cantidad de firmas requeridas, esperadas:${SIGNATURES_QUANTITY_LIMIT}, encontradas: ${totalOfSignatures}`);
        return;
    }
    const finalCertificates = validationResponse.report.signatures[0].certificates;
    if (finalCertificates.length < FINAL_CERTIFICATES_QUANTITY_LIMIT) {
        console.log(`No se encontró la cantidad de certificados necesarios, esperados ${FINAL_CERTIFICATES_QUANTITY_LIMIT}, encontrados:${finalCertificates.length} `);
        return;
    }
    var totalOfValidSubjects = 0;
    var digicertTimestampCertificateFound = false;
    var digicertCertificateFound = false;
    var attCertificateFound = false;

    for (var i = 0; i < finalCertificates.length; i++) {
        const subject = finalCertificates[i].certificate.subject.x500name;
        if (subject == "C=BO,O=Digicert,CN=Autoridad de Sellado de Tiempo Digicert") {
            totalOfValidSubjects++;
            if (digicertTimestampCertificateFound) {
                console.log("Se encontró un duplicado del certificado de sellado de tiempo de Digicert, no se puede continuar");
                return;
            } else {
                digicertTimestampCertificateFound = true;
            }
        } else if (subject == "C=BO,O=Digicert,CN=Entidad Certificadora Autorizada Digicert") {
            totalOfValidSubjects++;
            if (digicertCertificateFound) {
                console.log("Se encontró un duplicado del certificado de Digicert, no se puede continuar");
                return;
            } else {
                digicertCertificateFound = true;
            }
        } else if (subject == "C=BO,O=ATT,CN=Entidad Certificadora Raiz de Bolivia") {
            totalOfValidSubjects++;
            if (attCertificateFound) {
                console.log("Se encontró un duplicado del certificado de ATT, no se puede continuar");
                return;
            } else {
                attCertificateFound = true;
            }
        } else {
            console.log(`Certificado[${i}] con subject diferente,subject: ${subject}`);
        }
    }

    if (totalOfValidSubjects != TOTAL_OF_REQUIRED_VALID_SUBJECTS) {
        console.log(`No se encontró el total de subjects esperado entre los certificados,esperados: ${TOTAL_OF_REQUIRED_VALID_SUBJECTS}, encontrados: ${totalOfValidSubjects}`);
        return;
    }
    if (!digicertTimestampCertificateFound) {
        console.log("No se encontró un certificado de sellado de tiempo de Digicert, no se puede continuar");
        return;
    }
    if (!digicertCertificateFound) {
        console.log("No se encontró un certificado de Digicert, no se puede continuar");
        return;
    }
    if (!attCertificateFound) {
        console.log("No se encontró un certificado de ATT, no se puede continuar");
        return;
    }

    console.log("Validación de documento finalizada exitosamente");
}

async function validateFile() {
    const file = await storage.bucket('gs://electoralexact.appspot.com').file('reportes/1161/1161-2020-10-14 11:59:36.535.xml');
    //const file = await storage.bucket('gs://electoralexact.appspot.com').file('reportes/1161/xml con error.xml');
    //const file = await storage.bucket('gs://electoralexact.appspot.com').file('reportes/1161/xml_roto.xml');
    var targetFile = '';
    file.createReadStream()
        .on('data', (chunk) => targetFile += chunk)
        .on('end', () => sendFileToValidate(targetFile, file.name));
}

validateFile().catch(console.error);